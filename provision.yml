---
- hosts: all
  remote_user: root

  tasks:
    - name: "set hostname"
      hostname:
        name: fdroid-gitlab-runner

    - name: "authorized_keys: set up the standard admins"
      authorized_key:
        user: root
        key: "{{ item.key }}"
        state: present
        unique: yes
      with_list: "{{ authorized_keys }}"

    - name: "apt: install debian packages for apt setup"
      apt:
        name: "{{ item }}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:
        - apt-transport-https
        - apt-transport-tor
        - debian-archive-keyring
        - gnupg

    - name: "apk_key: Add Docker key"
      apt_key:
        url: https://download.docker.com/linux/debian/gpg
        state: present

    - name: "apt_repository: Add Docker apt repo"
      apt_repository:
        repo: 'deb [arch=amd64] https://download.docker.com/linux/{{ ansible_distribution | lower }} {{ ansible_distribution_release }} stable'
        state: present

    - name: "apk_key: Add GitLab key"
      apt_key:
        url: https://packages.gitlab.com/gpg.key
        state: present

    - name: "apt_repository: Add GitLab Runner apt repo"
      apt_repository:
        repo: 'deb https://packages.gitlab.com/runner/gitlab-runner/{{ ansible_distribution | lower }}/ {{ ansible_distribution_release }} main'
        state: present

    - name: "copy: script for updating with apt"
      copy:
        mode: 0700
        content: |
          #!/bin/sh

          set -x
          apt-get update
          apt-get -y dist-upgrade --download-only

          set -e
          apt-get -y upgrade
          apt-get dist-upgrade
          apt-get autoremove --purge
          apt-get clean
        dest: /root/update-all
      become: yes

    - name: "copy: apt pinning rule for gitlab-runner packages"
      copy:
        content: |
          Explanation: Prefer GitLab provided packages over the Debian native ones
          Package: gitlab-runner
          Pin: origin packages.gitlab.com
          Pin-Priority: 1001
        dest: /etc/apt/preferences.d/gitlab-runner.pref
      become: yes

    - name: "apt_repository: enable stretch-backports"
      apt_repository:
        repo: 'deb http://deb.debian.org/debian stretch-backports main'
        state: present
        update_cache: no
      become: yes

    - name: "apt: install debian packages"
      apt:
        name: "{{ item }}"
        state: latest
        autoclean: yes
        autoremove: yes
        install_recommends: no
      with_items:

        # useful utils
        - bash-completion
        - curl
        - emacs-nox
        - figlet
        - htop
        - iotop
        - less
        - ncdu
        - unattended-upgrades
        - vim
        - wget

        # full gitlab ci runner setup
        - docker-ce
        - gitlab-runner
        - python-docker

        # fdroid-website
        - openssh-client
        - python3-requests
        - rsync
        - screen

      become: yes

    - name: 'locale_gen: generate needed locales'
      locale_gen:
        name: "{{ item }}"
        state: present
      with_items:
        - de_AT.UTF-8
        - de_DE.UTF-8
        - en_US.UTF-8
        - en_GB.UTF-8

    - name: 'lineinfile: set locale to en_US.UTF-8'
      lineinfile:
        dest: "/etc/default/locale"
        line: "LANG=en_US.UTF-8"
      notify: reconfigure locales
      become: yes

    - name: "shell: set motd"
      shell: |
        echo > /etc/motd
        figlet gitlab-runner >> /etc/motd
        printf '\ncreated with https://gitlab.com/fdroid/fdroid-gitlab-runner\n\n' >> /etc/motd

    - name: "copy: deploy enable_kvm_nesting.sh to server"
      copy:
        mode: 0700
        src: enable_kvm_nesting.sh
        dest: /root/enable_kvm_nesting.sh
      become: yes

    - name: "command: enable kvm nesting"
      command: "/root/enable_kvm_nesting.sh"
      become: yes

    - name: "service: Ensure Docker service is installed and restarted"
      service:
        name: docker
        state: restarted
        enabled: yes

    - name: "docker_container: run gitlab-runner-docker-cleanup"
      docker_container:
        image: quay.io/gitlab/gitlab-runner-docker-cleanup
        name: gitlab-runner-docker-cleanup
        state: started
        restart: yes
        restart_policy: always
        env:
          LOW_FREE_SPACE: 3G
          EXPECTED_FREE_SPACE: 15G
          LOW_FREE_FILES_COUNT: 1048576
          EXPECTED_FREE_FILES_COUNT: 2097152
          DEFAULT_TTL: 10m
          USE_DF: 1
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock"

    - name: "command: list configured runners"
      command: gitlab-runner list
      register: configured_runners

    - name: "lineinfile: force concurrent number"
      lineinfile:
        dest: "/etc/gitlab-runner/config.toml"
        regexp: "^concurrent *= *.*"
        line: "concurrent = 4"
        create: yes

    - name: "command: gitlab-runner register"
      command: /usr/bin/gitlab-runner register
        --non-interactive
        --executor docker
        --docker-tlsverify=true
        --docker-privileged=true
        --docker-image "docker:dind"
        --tag-list "fdroid,privileged,32GB,KVM,kvm"
        --run-untagged=true
        --locked=false
        --registration-token "{{ item.registration_token }}"
        --url "{{ item.url }}"
        --name "fdroid-gitlab-runner-{{ item.name }}"
      loop: "{{ tokens }}"
      when: item.name|string not in configured_runners.stdout

    - name: "git: clone fdroid-website"
      git:
        repo: 'https://gitlab.com/fdroid/fdroid-website.git'
        dest: '/root/code/fdroid/fdroid-website'
        version: master
      become: yes

    - name: "known_hosts: trust staging.f-droid.org"
      known_hosts:
        path: /etc/ssh/ssh_known_hosts
        name: staging.f-droid.org
        key: "{{ lookup('file', 'staging.f-droid.org.known_hosts') }}"

    - name: "copy: ssh private key to rsync to staging.f-droid.org"
      copy:
        mode: 0600
        content: "{{ id_rsa }}"
        dest: /root/.ssh/id_rsa

    - name: "copy: ssh public key"
      copy:
        mode: 0644
        content: "{{ id_rsa_pub }}"
        dest: /root/.ssh/id_rsa.pub

    - name: "copy: keyring for verifying fdroid-website.git tags"
      copy:
        mode: 0400
        src: deploy-to-staging.f-droid.org-whitelist-keyring.gpg
        dest: /root/deploy-to-staging.f-droid.org-whitelist-keyring.gpg

    - name: "copy: script to build staging.f-droid.org in screen session"
      copy:
        mode: 0500
        src: deploy-to-staging.f-droid.org
        dest: /root/deploy-to-staging.f-droid.org

    - name: "copy: cron job to build staging.f-droid.org"
      copy:
        mode: 0755
        content: |
          #!/bin/sh
          set -e
          jobname=deploy-to-staging.f-droid.org
          if screen -list | grep --fixed-strings "${jobname}"; then
              exit 0
          fi
          cp /dev/null /tmp/${jobname}.log
          screen -d -m -L /tmp/${jobname}.log \
            -S ${jobname} \
            -t ${jobname} \
            /root/${jobname}
        dest: /etc/cron.hourly/deploy-to-staging-f-droid-org
      become: yes
